---
layout: full
homepage: true
disable_anchors: true
description: PHP CRUD operations project
---

A PHP project written in OOP which implements all of the CRUD functionality:

> - Display products
> - Add products
> - Delete products

<div class="row">
<div class="col-lg-6" markdown="1">

## Get started
{:.mt-lg-0}

- Fork and clone the repository from [HERE](https://gitlab.com/aivarsyoo/php-crud-operations)

Take a look at **Makefile** in the project root directory and use the shortcuts or proceed with the commands below.

### Local development
- Build the app container based on PHP
```
docker-compose build
```
- Pull rest of the images defined in docker-compose.yml and run the app environment on your machine
```
docker-compose up -d
```
- Nginx server is exposed to port 8080, so you can open it in your browser as localhost:8080 or 127.0.0.1:8080

### Latest release
If you wish to locally setup the latest release from Gitlab registry or deploy it manually to your server, the procedure is almost the same. You will need to use this command:
```
docker-compose -f docker-compose.deploy.yml up -d
```

### Quality assurance

Code goes through quality assurance when the changes are committed, but we can also test our code locally.

Enter php container from your terminal:
```
docker exec -it php bash
```
> - To lint the php part of the code:
```
vendor/bin/phpstan analyse
```
You can configure the settings for linting in ```src/phpstan.neon```
> - To run Unit & Integration tests (they are under the same directory):
```
vendor/bin/codecept run Unit
```
Configuration file ```src/tests/Unit.suite.yml```
> - To run Acceptance tests it uses another container for hosting Selenium grid server, make sure it is up and running
```
vendor/bin/codecept run Acceptance
```
Configuration file ```src/tests/Acceptance.suite.yml```

To review which parts of the application are tested well and which are not we can create code coverage. We can execute our tests to collect coverage report
```
vendor/bin/codecept run --coverage --coverage-xml --coverage-html
```
</div>
<div class="col-lg-6" markdown="1">

## Gitlab
{:.mt-lg-0}

The documented scenario here expects that you will use Gitlab as your code sharing platform.

### Repository structure

Two main branches - **master** and **deploy** are used for this project, other branches used for implementing features. We use master branch for having the latest and stable version of the project. And if we wish to show the features to the customer for approval or just test it online ourselves, we use the deploy branch where the code from master is merged and then automatically deployed to our server.

**pages** branch is used for writing this documentation. If you wish to add or change something, checkout to this branch, create a feature branch, make changes and then merge the changes.

### CI/CD pipelines

Repository uses ```.gitlab-ci.yml``` file to automatically create a pipeline when changes are commited to the branch. If you don't need to use a job created in the file just type a dot at the start of the job name like this ```.build-app``` .

It may not work for you if you are not a maintainer of this project or you have forked it, because you will need to set up variables in the project settings for Mysql database, docker login and deployment settings.

</div>
</div>

