I"�<p>A PHP project written in OOP which implements all of the CRUD functionality:</p>

<blockquote>
  <ul>
    <li>Display products</li>
    <li>Add products</li>
    <li>Delete products</li>
  </ul>
</blockquote>

<div class="row">
<div class="col-lg-6">

    <h2 class="mt-lg-0" id="get-started">Get started</h2>

    <ul>
      <li>Fork and clone the repository from <a href="https://gitlab.com/aivarsyoo/php-crud-operations">HERE</a></li>
    </ul>

    <p>Take a look at <strong>Makefile</strong> in the project root directory and use the shortcuts or proceed with the commands below.</p>

    <h3 id="local-development">Local development</h3>
    <ul>
      <li>Build the app container based on PHP
        <div class="language-plaintext highlighter-rouge"><div class="highlight"><pre class="highlight"><code>docker-compose build
</code></pre></div>        </div>
      </li>
      <li>Pull rest of the images defined in docker-compose.yml and run the app environment on your machine
        <div class="language-plaintext highlighter-rouge"><div class="highlight"><pre class="highlight"><code>docker-compose up -d
</code></pre></div>        </div>
      </li>
      <li>Nginx server is exposed to port 8080, so you can open it in your browser as localhost:8080 or 127.0.0.1:8080</li>
    </ul>

    <h3 id="latest-release">Latest release</h3>
    <p>If you wish to locally setup the latest release from Gitlab registry or deploy it manually to your server, the procedure is almost the same. You will need to use this command:</p>
    <div class="language-plaintext highlighter-rouge"><div class="highlight"><pre class="highlight"><code>docker-compose -f docker-compose.deploy.yml up -d
</code></pre></div>    </div>

    <h3 id="quality-assurance">Quality assurance</h3>

    <p>Code goes through quality assurance when the changes are committed, but we can also test our code locally.</p>

    <p>Enter php container from your terminal:</p>
    <div class="language-plaintext highlighter-rouge"><div class="highlight"><pre class="highlight"><code>docker exec -it php bash
</code></pre></div>    </div>
    <blockquote>
      <ul>
        <li>To lint the php part of the code:
          <div class="language-plaintext highlighter-rouge"><div class="highlight"><pre class="highlight"><code>vendor/bin/phpstan analyse
</code></pre></div>          </div>
          <p>You can configure the settings for linting in <code class="language-plaintext highlighter-rouge">src/phpstan.neon</code></p>
        </li>
        <li>To run Unit &amp; Integration tests (they are under the same directory):
          <div class="language-plaintext highlighter-rouge"><div class="highlight"><pre class="highlight"><code>vendor/bin/codecept run Unit
</code></pre></div>          </div>
          <p>Configuration file <code class="language-plaintext highlighter-rouge">src/tests/Unit.suite.yml</code></p>
        </li>
        <li>To run Acceptance tests it uses another container for hosting Selenium grid server, make sure it is up and running
          <div class="language-plaintext highlighter-rouge"><div class="highlight"><pre class="highlight"><code>vendor/bin/codecept run Acceptance
</code></pre></div>          </div>
          <p>Configuration file <code class="language-plaintext highlighter-rouge">src/tests/Acceptance.suite.yml</code></p>
        </li>
      </ul>
    </blockquote>

    <p>To review which parts of the application are tested well and which are not we can create code coverage. We can execute our tests to collect coverage report</p>
    <div class="language-plaintext highlighter-rouge"><div class="highlight"><pre class="highlight"><code>vendor/bin/codecept run --coverage --coverage-xml --coverage-html
</code></pre></div>    </div>
  </div>
<div class="col-lg-6">

    <h2 class="mt-lg-0" id="gitlab">Gitlab</h2>

    <p>The documented scenario here expects that you will use Gitlab as your code sharing platform.</p>

    <h3 id="repository-structure">Repository structure</h3>

    <p>Two main branches - <strong>master</strong> and <strong>deploy</strong> are used for this project, other branches used for implementing features. We use master branch for having the latest and stable version of the project. And if we wish to show the features to the customer for approval or just test it online ourselves, we use the deploy branch where the code from master is merged and then automatically deployed to our server.</p>

    <p><strong>pages</strong> branch is used for writing this documentation. If you wish to add or change something, checkout to this branch, create a feature branch, make changes and then merge the changes.</p>

    <h3 id="cicd-pipelines">CI/CD pipelines</h3>

    <p>Repository uses <code class="language-plaintext highlighter-rouge">.gitlab-ci.yml</code> file to automatically create a pipeline when changes are commited to the branch. If you don’t need to use a job created in the file just type a dot at the start of the job name like this <code class="language-plaintext highlighter-rouge">.build-app</code> .</p>

    <p>It may not work for you if you are not a maintainer of this project or you have forked it, because you will need to set up variables in the project settings for Mysql database, docker login and deployment settings.</p>

  </div>
</div>

:ET