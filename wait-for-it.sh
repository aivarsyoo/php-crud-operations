#!/bin/sh

wait-for-it php:9000 -t 60
wait-for-it mysql:3306 -t 60
wait-for-it chrome:4444 -t 60
wait-for-it web:80 -t 60