-- phpMyAdmin SQL Dump
-- version 4.9.7
-- https://www.phpmyadmin.net/
--
-- Host: localhost:8889
-- Generation Time: Jun 15, 2022 at 03:17 AM
-- Server version: 5.7.32
-- PHP Version: 7.4.12

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";

--
-- Database: `scandiweb_test`
--

-- --------------------------------------------------------

--
-- Table structure for table `products`
--

CREATE TABLE `products` (
  `sku` varchar(255) NOT NULL,
  `type` varchar(255) NOT NULL,
  `name` varchar(255) NOT NULL,
  `price` decimal(10,2) NOT NULL,
  `size` varchar(11) DEFAULT NULL,
  `height` varchar(11) DEFAULT NULL,
  `width` varchar(11) DEFAULT NULL,
  `length` varchar(11) DEFAULT NULL,
  `weight` varchar(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `products`
--

INSERT INTO `products` (`sku`, `type`, `name`, `price`, `size`, `height`, `width`, `length`, `weight`) VALUES
('BOO4986', 'Book', 'Harry Potter', '13.50', NULL, NULL, NULL, NULL, '0.7'),
('DVD9456', 'DVD', 'Lord Of The Rings', '7.30', '2100', NULL, NULL, NULL, NULL),
('FUR3056', 'Furniture', 'Nice Chair', '24.60', NULL, '70', '60', '89', NULL);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `products`
--
ALTER TABLE `products`
  ADD PRIMARY KEY (`sku`);
