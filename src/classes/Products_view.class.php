<?php

class Products_view extends Products
{
    public $product_types = array();
    public $type_products = array();
    public $all_products = array();

    public function set_product_types($product_types)
    {
        foreach ($product_types as $type) {
            array_push($this->product_types, $type);
        }
    }

    public function get_type_products()
    {
        foreach ($this->product_types as $type) {
            array_push($this->type_products, $this->get_products($type));
        }
    }

    public function populate_all_products()
    {
        foreach ($this->type_products as $one_type_products) {
            if (!empty($one_type_products)) {
                foreach ($one_type_products as $product) {
                    array_push($this->all_products, $product);
                }
            }
        }
        asort($this->all_products);
    }

    public function show_product()
    {
        foreach ($this->all_products as $product) {
            $show = new $product['type']();
            $show->set_attributes($product);
            $show->show_product();
        }
    }
}
