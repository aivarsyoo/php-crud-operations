<?php

class Validator extends Helper
{
    function __construct($attributes, $must_have_attributes)
    {
        foreach ($must_have_attributes as $attribute) {
            if (!array_key_exists($attribute, $attributes)) {
                $this->send_400("Product $attribute is required");
            }
        }
    }

    public function validate_sku($key, $value)
    {
        if (empty($value)) {
            $this->send_400("Product $key is required");
        }

        if (strlen($value) < 5) {
            $this->send_400("Product $key should be at least 5 characters");
        }
    }

    public function validate_name($key, $value)
    {
        if (empty($value)) {
            $this->send_400("Product $key is required");
        }
        if (strlen($value) < 6) {
            $this->send_400("Product $key should be at least 6 characters");
        }
    }

    public function validate_price($key, $value)
    {
        if (empty($value)) {
            $this->send_400("Product $key is required");
        }
        if ($value < 0) {
            $this->send_400("Product $key should be a positive number");
        }
        if (!is_numeric($value)) {
            $this->send_400("Product $key should only contain numbers");
        }
    }

    public function validate_type($key, $value)
    {
        if (empty($value)) {
            $this->send_400("Product $key is required");
        }
    }

    public function validate_size($key, $value)
    {
        if (empty($value)) {
            $this->send_400("DVD $key is required");
        }
        if ($value < 0) {
            $this->send_400("DVD $key should be a positive number");
        }
        if (!is_numeric($value)) {
            $this->send_400("DVD $key should only contain numbers");
        }
    }

    public function validate_weight($key, $value)
    {
        if (empty($value)) {
            $this->send_400("Product $key for the book is required");
        }
        if ($value < 0) {
            $this->send_400("Product$key for the book should be a positive number");
        }
        if (!is_numeric($value)) {
            $this->send_400("Product $key for the book should only contain numbers");
        }
    }

    public function validate_height($key, $value)
    {
        if (empty($value)) {
            $this->send_400("Product $key for the furniture is required");
        }
        if ($value < 0) {
            $this->send_400("Product $key for the furniture should be a positive number");
        }
        if (!is_numeric($value)) {
            $this->send_400("Product $key for the furniture should only contain numbers");
        }
    }

    public function validate_length($key, $value)
    {
        if (empty($value)) {
            $this->send_400("Product $key for the furniture is required");
        }
        if ($value < 0) {
            $this->send_400("Product $key for the furniture should be a positive number");
        }
        if (!is_numeric($value)) {
            $this->send_400("Product $key for the furniture should only contain numbers");
        }
    }

    public function validate_width($key, $value)
    {
        if (empty($value)) {
            $this->send_400("Product $key for the furniture is required");
        }
        if ($value < 0) {
            $this->send_400("Product $key for the furniture should be a positive number");
        }
        if (!is_numeric($value)) {
            $this->send_400("Product $key for the furniture should only contain numbers");
        }
    }
}
