<?php

class Db_conn
{

    private $servername;
    private $username;
    private $password;
    private $db_name;

    protected function connect()
    {
        $this->servername = "mysql";
        $this->username = "db_user";
        $this->password = ".mypwd";
        $this->db_name = "scandiweb_test";

        $conn = new \mysqli($this->servername, $this->username, $this->password, $this->db_name);

        if ($conn->connect_error) {
            die("<h1>System under maintenance</h1>");
        }
        //echo "Database Connected Successfully";
        return $conn;
    }
}
