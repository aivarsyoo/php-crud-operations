<?php

class Products extends Db_conn
{
    protected $keys;
    protected $values;

    public function prepare_query($attributes)
    {
        $keys_array = array();
        $values_array = array();
        foreach ($attributes as $key => $val) {
            array_push($keys_array, $key);
            array_push($values_array, "'$val'");
        }
        $this->keys = implode(",", $keys_array);
        $this->values = implode(",", $values_array);
    }

    public function get_products($type)
    {
        $sql = "SELECT * FROM products WHERE type='$type'";
        $result = $this->connect()->query($sql);
        if (!$result) {
            $response = new Helper();
            $response->send_400("Failed to fetch the data");
        }
        $num_rows = $result->num_rows;
        if ($num_rows > 0) {
            while ($row = $result->fetch_assoc()) {
                $data[] = $row;
            }
            return $data;
        }
    }

    public function delete_products($values)
    {
        $sql = "DELETE FROM products WHERE sku IN ($values)";
        $result = $this->connect()->query($sql);
        if (!$result) {
            $response = new Helper();
            $response->send_400("Failed to delete product/-s");
        } else {
            $response = new Helper();
            $response->send_200("Product/-s deleted successfully");
        }
    }

    public function check_record($key, $value)
    {
        $sql = "SELECT * FROM products WHERE $key='$value'";
        $result = $this->connect()->query($sql);
        if (!$result) {
            $response = new Helper();
            $response->send_400("Failed to fetch the data");
        }
        $num_rows = $result->num_rows;
        if ($num_rows > 0) {
            return true;
        } else {
            return false;
        }
    }

    public function add_product($keys, $values)
    {
        $sql = "INSERT INTO products($keys) 
        VALUES($values)";
        $result = $this->connect()->query($sql);
        if (!$result) {
            $response = new Helper();
            $response->send_400("Failed to add the product");
        } else {
            $response = new Helper();
            $response->send_200("Product added successfully");
        }
    }
}
