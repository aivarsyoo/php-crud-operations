<?php

class Products_contr extends Products
{

    private $sku;
    private $type;
    private $sku_array;

    public function set_sku($sku)
    {
        $this->sku = $sku;
    }

    public function set_sku_array($sku_array)
    {
        $this->sku_array = $sku_array;
    }

    public function set_type($type)
    {
        $this->type = $type;
    }

    public function remove_product()
    {
        $this->prepare_query($this->sku_array);
        $this->delete_products($this->values);
    }

    public function check_attr_exists($key)
    {
        $exists = $this->check_record($key, $this->$key);
        if ($exists == true) {
            $error = new Helper();
            $error->send_400("Product with this $key already exists");
        }
    }

    public function prepare_product()
    {
        $prepare = new $this->type();
        $prepare->set_attributes($_POST);
        $prepare->validate();
        $prepare->add_product();
    }

    public function post_product($attributes)
    {
        $this->prepare_query($attributes);
        $this->add_product($this->keys, $this->values);
    }
}
