<?php

class Helper
{
    public function send_400($error_message)
    {
        http_response_code(400);
        header('Content-Type: application/json');
        $response = ["info" => $error_message];
        echo json_encode($response);
        exit();
    }

    public function send_200($message)
    {
        http_response_code(200);
        header('Content-Type: application/json');
        $response = ["info" => $message];
        echo json_encode($response);
    }
}
