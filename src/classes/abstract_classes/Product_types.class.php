<?php

abstract class Product_types
{
    public $attributes;
    public $must_have_attributes = array('sku', 'type', 'name', 'price');
    public $sku;
    public $type;
    public $name;
    public $price;
    public $size;
    public $height;
    public $width;
    public $length;
    public $weight;
    public $specific_attr;

    abstract protected function set_attributes($attributes);
    abstract protected function show_product();
    abstract protected function validate();
    abstract protected function add_product();
}
