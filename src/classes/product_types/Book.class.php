<?php

class Book extends Product_types
{

    public function set_attributes($attributes)
    {
        $this->attributes = $attributes;
        foreach ($this->attributes as $key => $value) {
            $this->$key = $value;
        }
        array_push($this->must_have_attributes, 'weight');
    }

    public function show_product()
    {
        $this->specific_attr = "<p>Weight: " . $this->weight . " KG</p>";
        require __DIR__ . '/../../templates/product_template.php';
    }

    public function validate()
    {
        $validator = new Validator($this->attributes, $this->must_have_attributes);
        foreach ($this->attributes as $key => $value) {
            $func_name = "validate_" . $key;
            $validator->$func_name($key, $value);
        }
    }

    public function add_product()
    {
        $add = new Products_contr();
        $add->post_product($this->attributes);
    }
}
