<?php

require __DIR__.'/../vendor/autoload.php';

if (!empty($_POST['type'])) {
    $add = new Products_contr();
    $add->set_type($_POST['type']);
    $add->set_sku($_POST['sku']);
    $add->check_attr_exists('sku');
    $add->prepare_product();
} else {
    $error = new Helper();
    $error->send_400("Please, submit required data");
}
