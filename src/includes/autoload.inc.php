<?php

function autoload($class_name)
{
    $array_paths = array(
        'classes',
        'classes/abstract_classes',
        'classes/product_types',
        'classes/db',
        '../classes',
        '../classes/abstract_classes',
        '../classes/product_types',
        '../classes/db',
        '../../classes',
        '../../classes/abstract_classes',
        '../../classes/product_types',
        '../../classes/db'
    );

    foreach ($array_paths as $path) {
        $file = sprintf('%s/%s.class.php', $path, $class_name);
        if (is_file($file)) {
            include_once $file;
        }
    }
}

spl_autoload_register('autoload');
