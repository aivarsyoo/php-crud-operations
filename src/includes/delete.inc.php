<?php

require __DIR__.'/../vendor/autoload.php';

if (!empty($_POST['products'])) {
    $delete = new Products_contr();
    $delete->set_sku_array($_POST['products']);
    $delete->remove_product();
} else {
    $error = new Helper();
    $error->send_400("Select products to delete");
}
