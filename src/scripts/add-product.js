export default async function AddProduct() {
  const form = this;
  let conn = await fetch("includes/add.inc.php", {
    method: "POST",
    body: new FormData(form),
  });
  let res = await conn.json();
  console.log(res);
  if (conn.ok) {
    location.href = "./";
  } else {
    invalidForm(res.info);
  }
}

function invalidForm(message) {
  const errorTxt = document.getElementById("add-product-message");
  errorTxt.innerHTML = message;
  errorTxt.classList.add("visible");
}
