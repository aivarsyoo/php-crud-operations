export function checkIfProductsExist() {
  const product = document.querySelector(".product");
  const emptyListText = document.getElementById("empty-list-txt");
  if (!product) {
    emptyListText.classList.add("visible");
  }
}
