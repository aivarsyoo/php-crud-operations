import { checkIfProductsExist } from "./functions.js";

export default async function DeleteProducts() {
  const form = this;
  let conn = await fetch("includes/delete.inc.php", {
    method: "POST",
    body: new FormData(form),
  });
  let res = await conn.json();
  console.log(res);
  if (conn.ok) {
    removeProducts();
    checkIfProductsExist();
    showMessage(res.info);
  } else {
    showMessage(res.info);
  }
}

function removeProducts() {
  const checkboxes = document.querySelectorAll(".delete-checkbox");
  checkboxes.forEach((checkbox) => {
    const product = document.getElementById(checkbox.value);
    if (checkbox.checked) {
      product.remove();
    }
  });
}

function showMessage(message) {
  const msgContainer = document.getElementById("products-message");
  const msgTxt = document.querySelector("#products-message p");
  msgTxt.innerHTML = message;
  msgContainer.classList.remove("products-message-animate");
  setTimeout(() => {
    msgContainer.classList.add("products-message-animate");
  }, 100);
}
