import DeleteProducts from "./delete-products.js";
import SwitchProduct from "./switcher.js";
import AddProduct from "./add-product.js";
import { checkIfProductsExist } from "./functions.js";

document.addEventListener("DOMContentLoaded", function () {
  const deleteForm = document.getElementById("delete_product_form");
  if (deleteForm) {
    deleteForm.addEventListener("submit", DeleteProducts);
    checkIfProductsExist();
  }

  const switcher = document.getElementById("productType");
  if (switcher) {
    switcher.addEventListener("change", SwitchProduct);
  }

  const addForm = document.getElementById("product_form");
  if (addForm) {
    addForm.addEventListener("submit", AddProduct);
  }
});
