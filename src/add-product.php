<?php
ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);
?>

<!DOCTYPE html>
<html>

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>Product Add</title>
    <meta name="description" content="">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="styles/css/main.css">
</head>

<body>

    <form onsubmit="return false" id="product_form">

        <header>
            <h1>Product Add</h1>
            <div>
                <input type="submit" value="Save" class="green-btn">
                <a href="./" class="red-btn">Cancel</a>
            </div>
        </header>

        <main id="add-main">

            <section id="product-add-container">
                <p id="add-product-message">name is required</p>
                <div class="attributes">
                    <div class="attributes__inner">
                        <label for="sku">SKU</label>
                        <input type="text" id="sku" name="sku" minlength="5" />
                        <label for="name">Name</label>
                        <input type="text" id="name" name="name" minlength="6" />
                        <label for="price">Price ($)</label>
                        <input type="number" id="price" name="price" min="0.01" step="0.01" />
                    </div>
                </div>

                <div class="switcher">
                    <label for="type">Product type</label>
                    <select name="type" id="productType">
                        <option value="">Select type</option>
                        <option value="DVD" id="DVD">DVD</option>
                        <option value="Book" id="Book">Book</option>
                        <option value="Furniture" id="Furniture">Furniture</option>
                    </select>
                </div>

                <div class="attributes">
                    <div class="attributes__inner" id="spec-attr"></div>
                    <template data-type="DVD">
                        <label for="size">Size (MB)</label>
                        <input type="number" id="size" name="size" min="1.00" step="1">
                        <p>Please provide DVD size in MB</p>
                    </template>

                    <template data-type="Furniture">
                        <label for="height">Height (CM)</label>
                        <input type="number" id="height" name="height" min="1.00" step="1">
                        <label for="width">Width (CM)</label>
                        <input type="number" id="width" name="width" min="1.00" step="1">
                        <label for="length">Length (CM)</label>
                        <input type="number" id="length" name="length" min="1.00" step="1">
                        <p>Please provide dimensions in HxWxL format</p>
                    </template>

                    <template data-type="Book">
                        <label for="weight">Weight (KG)</label>
                        <input type="number" id="weight" name="weight" min="0.1" step="0.1">
                        <p>Please provide weight in KG</p>
                    </template>
                </div>

            </section>
        </main>

    </form>

    <?php
    require_once __DIR__ . '/templates/footer.php';
    ?>
</body>

</html>