<?php
include "c3.php";
ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);
require __DIR__.'/vendor/autoload.php';
$product_types = ["DVD", "Book", "Furniture"];
//include __DIR__ . '/includes/autoload.inc.php';
?>

<!DOCTYPE html>
<html>

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>Product List</title>
    <meta name="description" content="">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="styles/css/main.css">
</head>

<body>
    <form onsubmit="return false" id="delete_product_form">
        <header>
            <h1>Product List</h1>
            <div>
                <a href="add-product" id="add-product-btn" class="green-btn">ADD</a>
                <input type="submit" id="delete-product-btn" value="MASS DELETE" class="red-btn">
            </div>
        </header>

        <main>
            <h2 id="empty-list-txt">No products have been added so far</h2>
            <div id="products-message">
                <p>Response message</p>
            </div>
            <?php
            $products = new Products_view();
            $products->set_product_types($product_types);
            $products->get_type_products();
            $products->populate_all_products();
            $products->show_product();
            ?>
        </main>
    </form>

    <?php
    require_once __DIR__ . '/templates/footer.php';
    ?>
</body>

</html>