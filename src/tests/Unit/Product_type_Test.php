<?php

use PHPUnit\Framework\TestCase;

class Products_type_Test extends TestCase
{
    public function test_product_type()
    {
        $book = new Book();
        $book->set_attributes([
            "sku" => "BOOK871",
            "name" => "Game of Thrones",
            "price" => "12.50",
            "type" => "Book",
            "weight" => "0.9"
        ]);

        $this->assertContains("weight", $book->must_have_attributes);

        foreach ($book->attributes as $attribute) {
            $this->assertNotEmpty($attribute);
        }

        $book->show_product();
        $this->assertFileExists(__DIR__ . '/../../templates/product_template.php');

        $validator = new Validator($book->attributes, $book->must_have_attributes);
        foreach ($book->attributes as $key => $value) {
            $this->assertNotEmpty($key);
            $this->assertNotEmpty($value);
            $func_name = "validate_" . $key;
            $validator->$func_name($key, $value);
        }
    }
}
