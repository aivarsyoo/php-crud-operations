<?php

use PHPUnit\Framework\TestCase;

class Products_view_Test extends TestCase
{

    public function test_show_products()
    {

        $products = new Products_view();
        $products->set_product_types(["DVD", "Book", "Furniture"]);
        $this->assertNotEmpty($products->product_types);

        // create sample products as db would retrieve
        $products->type_products = [
            [[
                "sku" => "BOO4986",
                "type" => "Book",
                "name" => "Harry Potter",
                "price" => "13.50",
                "size" => "",
                "height" => "",
                "width" => "",
                "length" => "",
                "weight" => "0.7"
            ]],
            [[
                "sku" => "DVD9456",
                "type" => "DVD",
                "name" => "Lord Of The Rings",
                "price" => "7.30",
                "size" => "2100",
                "height" => "",
                "width" => "",
                "length" => "",
                "weight" => ""
            ]]
        ];

        $products->populate_all_products();

        foreach ($products->all_products as $product) {
            $this->assertArrayHasKey('sku', $product);
            $this->assertArrayHasKey('type', $product);
            $this->assertArrayHasKey('name', $product);
            $this->assertArrayHasKey('price', $product);
            $this->assertArrayHasKey('size', $product);
            $this->assertArrayHasKey('height', $product);
            $this->assertArrayHasKey('width', $product);
            $this->assertArrayHasKey('length', $product);
            $this->assertArrayHasKey('weight', $product);
        }

        $products->show_product();
    }
}
