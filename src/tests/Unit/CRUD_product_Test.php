<?php

//namespace Tests\Unit;

use Tests\Support\UnitTester;

class CRUD_product_Test extends \Codeception\Test\Unit
{

    protected UnitTester $tester;

    public function test_add_DVD()
    {
        $_POST['sku'] = 'DVD1234';
        $_POST['name'] = 'Lord of The Rings';
        $_POST['price'] = '9.50';
        $_POST['type'] = 'DVD';
        $_POST['size'] = '9800';

        $add = new Products_contr();
        $add->set_type($_POST['type']);
        $add->set_sku($_POST['sku']);
        $add->check_attr_exists('sku');
        $add->prepare_product();

        $this->tester->seeInDatabase('products', ['sku' => 'DVD1234']);
    }

    public function test_delete_DVD()
    {
        $_POST['products'] = ['DVD1234'];

        $delete = new Products_contr();
        $delete->set_sku_array($_POST['products']);
        $delete->remove_product();

        $this->tester->dontSeeInDatabase('products', ['sku' => 'DVD1234']);
    }
}
