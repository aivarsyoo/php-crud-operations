<?php

namespace Tests\Acceptance;

use Tests\Support\AcceptanceTester;

class AddInvalidProductCest
{
    public function addInvalidFurniture(AcceptanceTester $I)
    {
        $I->amOnPage('/');
        $I->click('ADD');
        $I->seeElement('#product_form');
        $I->fillField('#sku', 'InvalidInput');
        $I->fillField('#name', 'InvalidName22');
        $I->fillField('#price', 'E');
        $I->selectOption('#productType','Book');
        $I->fillField('#weight','E');
        $I->click('Save');
    }

    public function cantFindInvalidProduct(AcceptanceTester $I){
        $I->amOnPage('/');
        $I->dontSeeElement("Invalid_Input");
    }
}
