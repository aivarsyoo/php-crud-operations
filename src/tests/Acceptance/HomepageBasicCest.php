<?php

namespace Tests\Acceptance;

use Tests\Support\AcceptanceTester;

class HomepageBasicCest
{
    public function checkHomepageBasic(AcceptanceTester $I)
    {
        $I->amOnPage('/');
        $I->click('MASS DELETE');
        $I->click('ADD');
    }

    public function checkProductBasic(AcceptanceTester $I)
    {
        $I->amOnPage('/');
        $I->click('ADD');
        $I->seeElement('#product_form');
        $I->seeElement('#sku');
        $I->seeElement('#name');
        $I->seeElement('#price');
        $I->seeElement('#productType');
    }
}
