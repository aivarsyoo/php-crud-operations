<?php

namespace Tests\Acceptance;

use Tests\Support\AcceptanceTester;

class DeleteAllProductsCest
{
    public function deleteProducts(AcceptanceTester $I)
    {
        $I->amOnPage('/');
        $I->seeElement('.delete-checkbox');
        $I->executeJS('
        checkboxes = document.getElementsByClassName("delete-checkbox");
            for (i=0; i<checkboxes.length;i++){
                checkboxes[i].checked=true;
            }
        ');
        $I->click("MASS DELETE");
    }

    public function checkProducts(AcceptanceTester $I)
    {
        $I->amOnPage('/');
        $I->waitForElementNotVisible('.delete-checkbox');
    }
}
