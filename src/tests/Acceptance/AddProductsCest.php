<?php

namespace Tests\Acceptance;

use Tests\Support\AcceptanceTester;

class AddProductsCest
{
    public function addDVD(AcceptanceTester $I)
    {
        $I->amOnPage('/');
        $I->click('ADD');
        $I->seeElement('#product_form');
        $I->fillField('#sku', 'SKUTest000');
        $I->fillField('#name', 'NameTest000');
        $I->fillField('#price', '25');
        $I->waitForElement('#productType');
        $I->selectOption('#productType','DVD');
        $I->waitForElement('#size');
        $I->fillField('#size','200');
        $I->click('Save');
    }

    public function addBook(AcceptanceTester $I)
    {
        $I->amOnPage('/');
        $I->click('ADD');
        $I->seeElement('#product_form');
        $I->fillField('#sku', 'SKUTest001');
        $I->fillField('#name', 'NameTest001');
        $I->fillField('#price', '25');
        $I->waitForElement('#productType');
        $I->selectOption('#productType','Book');
        $I->waitForElement('#weight');
        $I->fillField('#weight','200');
        $I->click('Save');
    }

    public function addFurniture(AcceptanceTester $I)
    {
        $I->amOnPage('/');
        $I->click('ADD');
        $I->seeElement('#product_form');
        $I->fillField('#sku', 'SKUTest002');
        $I->fillField('#name', 'NameTest002');
        $I->fillField('#price', '25');
        $I->waitForElement('#productType');
        $I->selectOption('#productType','Furniture');
        $I->waitForElement('#height');
        $I->waitForElement('#width');
        $I->waitForElement('#length');
        $I->fillField('#height','200');
        $I->fillField('#width','200');
        $I->fillField('#length','200');
        $I->click('Save');
    }

    public function checkForBook(AcceptanceTester $I)
    {
        $I->amOnPage('/');
        $I->waitForText('NameTest000');
    }

    public function checkForDVD(AcceptanceTester $I)
    {
        $I->amOnPage('/');
        $I->waitForText('NameTest001');
    }

    public function checkForFurniture(AcceptanceTester $I)
    {
        $I->amOnPage('/');
        $I->waitForText('NameTest002');
    }
}
