# DOCKER COMMANDS

up:
	docker-compose up

core-up:
	docker-compose up -d

build:
	docker-compose build

pull:
	docker-compose pull

down:
	docker-compose down --remove-orphans

down-rm-volumes:
	docker-compose down -v

logs:
	docker-compose logs -f

core-logs:
	docker-compose -f logs -f --tail 100

full-rebuild:
	docker-compose build --pull --no-cache

deploy:
	docker-compose -f docker-compose.deploy.yml up -d

deploy-down:
	docker-compose -f docker-compose.deploy.yml down --remove-orphans